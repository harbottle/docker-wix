# docker-wix

A CentOS 7-based Docker image for [WiX Toolset](http://wixtoolset.org/).

## Introduction

[WiX](http://wixtoolset.org/) is the industry-standard tool for building
Windows Installer packages (`.msi` files). This CentOS 7-based Docker image
contains [Wine](https://www.winehq.org/) and the WiX Toolset, allowing you to
build Windows packages right from your Linux command line or your favourite
CI tool.

Not all the WiX tools are guaranteed to work, but the tools for creation of
`.msi` files are certainly functional.

## Basic usage

First ensure you have Docker running on your system.

```bash
# Compile your project using the WiX candle tool
docker run -v $PWD:/mnt/workspace -it harbottle/wix candle MyProject.wxs

# Create your MSI using the WiX light tool
docker run -v $PWD:/mnt/workspace -it harbottle/wix light -sval MyProject.wixobj
```

## Example

This project includes an example you can try.

```bash
# Clone this repo
git clone https://gitlab.com/harbottle/docker-wix

# Change to example directory
cd docker-wix/example

# Compile your project using the WiX candle tool
# (the image will take a while to download the first time you use it)
docker run -v $PWD:/mnt/workspace -it harbottle/wix candle example.wxs

# Create your MSI using the WiX light tool
docker run -v $PWD:/mnt/workspace -it harbottle/wix light -sval example.wixobj
```

This will output a Windows Installer file `example.msi` containing
`example.exe`. If you run this installer package on Windows, `example.exe` will
be installed:

```
C:\>"c:\Program Files\Example\example.exe"
Hello World!
```

# GitLab-CI

The docker image can be used with any continous integration product for
automated `.msi` builds. This project includes a
[GitLab-CI config file](.gitlab-ci.yml) that runs the above example and
produces an example `.msi` file
[artifact for download](https://gitlab.com/harbottle/docker-wix/-/jobs/artifacts/master/browse/example?job=build%3Amsi).

## Credits

Inspiration for this project and the example used comes from
[sucha/wix-toolset](https://github.com/suchja/wix-toolset).